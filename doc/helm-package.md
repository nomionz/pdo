# Beey Helm package

## Prerequisites

- Kubernetes cluster 1.21+

## Components

### Scribe

* The actual Beey web-application server

### SPP

* SPP-server for automatic punctuation and other postprocessing tasks

### apps-voice

* Voice Beey-app

### Elastic
* Elasticsearch cluster
### Kibana
* Kibana for Elasticsearch

### MySQL InnoDBCluster
* MySQL InnoDBCluster for Beey database

### Traefik

* Optional service proxy

# Spin up

There are two possible scenarios for the databases:
  1. Install MySQL to the kubernetes cluster
  2. MySQL is external

## Install MySQL to the kubernetes cluster

### 1. Add mysql operator to the cluster with helm

  ```bash
  helm repo add mysql-operator https://mysqloperator.github.io/charts/
  ```
  
### 2. Install the mysql operator in the namespace!

  ```bash
    helm install mysql-operator mysql-operator/mysql-operator --namespace databases --create-namespace
  ```
### 3. Add and install elasticsearch operator to the cluster with helm
  
  ```bash
  helm repo add elastic https://helm.elastic.co
  helm install elasticsearch-operator elastic/eck-operator --namespace databases --create-namespace
  ```
## MySQL is external

### 1. Create mysql connection string k8s secret with the **db-connection-string** name

example:

```bash
connection_string="database connection string"
kubectl create secret generic db-connection-string --from-literal=password=$connection_string
```

### 2. Add and install elasticsearch operator to the cluster with helm
  
  ```bash
  helm repo add elastic https://helm.elastic.co
  helm install elasticsearch-operator elastic/eck-operator --namespace databases --create-namespace
  ```

## Helm 

--set-file will create a configmap with the file content

    helm upgrade --install beey <path_to_helm_package> \
    --set-file scribe.config.scribeSettingsXML=./Settings.xml \
    --set-file spp.config.sppSetupJson=./spp_setup.json \
    --set-file scribe.config.fakeng2storeJson=./fakeng2store.json

To apply the changes in Settings.xml or spp_setup.json, simply use the command above, kubernetes will automatically detect the changes and apply them.

**But be aware that this will result in a forced reload of the pod.**


## Configuration

List of selected configuration options

| Parameter | Description | Default |
| --- | --- | --- |
| `scribe.config.scribeSettingsXML` | Scribe settings.xml file | `""` |
| `scribe.config.fakeng2storeJson` | Scribe fakeng2store.json file | `""` |
| `spp.config.sppSetupJson` | SPP spp_setup.json file | `""` |
| `mysql.enabled` | Install MySQL to the cluster | `true` |
| `mysql.version` | MySQL version | `"8.0.31"` |
| `mysql.namespace` | MySQL namespace | `databases` |
| `mysql.credentials.rootUser` | MySQL root user | `"root"` |
| `mysql.credentials.password` | MySQL root password | `"password"` |
| `mysql.tlsUseSelfSigned` | Use self-signed certificate for MySQL | `true` |
| `mysql.serverInstances` | Number of MySQL instances | `1` |
| `mysql.routerInstances` | Number of MySQL routerInstances | `1` |
| `elastic.enabled` | Install Elasticsearch to the cluster | `true` |
| `elastic.version` | Elasticsearch version | `"7.14.2"` |
| `elastic.namespace` | Elasticsearch namespace | `databases` |
| `elastic.node_count` | Number of Elasticsearch nodes | `1` |
| `elastic.env` | Elasticsearch environment variables | `ES_JAVA_OPTS "-Xms512m -Xmx512m"` |
| `kibana.enabled` | Install Kibana to the cluster | `true` |
| `kibana.version` | Kibana version | `"7.14.2"` |
| `kibana.count` | Number of Kibana instances | `1` |
