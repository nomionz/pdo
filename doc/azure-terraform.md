## How to use it

### Prerequisites

- azure-cli
- terraform

### Steps

Login to azure
```bash
az login
```

Run this script to create a storage account and a container to store the terraform state file.
Should be run with **source** command to export the environment variables.

```bash
source scripts/remote_state.sh
```
After running the script, the following output should be displayed:
```bash
Successfully created storage account and container 
Run the following commands to initialize the backend: 
terraform init -backend-config='backend.cfg'
terraform workspace new/select <name>
terraform import azurerm_resource_group.newton-rg <id>
```

Initialize the terraform backend
```bash
terraform init -backend-config='backend.cfg'
```

Create a new workspace **dev** or **prod**
```bash
terraform workspace new dev
```

Import the resource group created by the **remote_state.sh** script
```bash 
terraform import azurerm_resource_group.newton-rg <id>
```

There could occur an error, because of the next piece of code in the **main.tf** file:
```ruby
provider "kubernetes" {
  host = module.k8s.kube_config.0.host
  client_certificate = base64decode(module.k8s.kube_config.0.client_certificate)
  client_key = base64decode(module.k8s.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(module.k8s.kube_config.0.cluster_ca_certificate)
}
```
**provider "kubernetes"** is used to create the k8s secret for database connection. To resolve the error just comment all inside the **provider "kubernetes"** block and run **terrafrom import** again.

Uncomment the **provider "kubernetes"** block and deploy the infrastructure.
```bash
terraform apply
```
