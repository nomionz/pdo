# Dokumentace automatizavane prenositelne instalace web aplikace pro rozpoznavani reci v cloudovech prostredich 

## Pro koho
Dokumentace je urcena pro firmu je zamerena na vyvojare a spravce infrastruktury.

## Struktura
Struktura moje dokumentaci je pomerne jednoducha je to postup s prikazy. 
Dokumentaci budou dve , prvni je pro **terraform** druha je pro **helm** sablony pro **kubernetes**.


1. Na zacatku dokumentaci jsou evedene tak zvane **prerequisites** neboli seznam utilit, ktere je potreba nastavit. 
V zadnem pripade nebudu dokumentavat to jak ty utility nastavit a to z toho duvodu, ze je dost mozne ze postup jejich nastaveni se bude menit.
2. Kratky seznam aplikace nebo sluzeb, ktere budou nasazene po uspesnem spusteni prikazu.
4. Dokumentace dal pokracuje tim jake prikazy v jakem poradi be meli byt spustene pro uspesne nasazeni s male komentare ode mne jako napriklad obarvene upozorneni s vykricniky pro kriticke dulezite prikazy.

Pro **helm** bude tabulka s parametry a ktere hodnoty jsou defaultni

## Dokumentaci
[helm balicek dokumentace](doc/helm-package.md)

[terraform dokumntace](doc/azure-terraform.md)
