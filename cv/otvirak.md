# Otvírák na pivo

## Zadani
- Zkuste napsat "koncept".
- Zkuste napsat pracovní postup pro otevření piva.
- Zkuste napsat referenci.
- Zkuste udělat katalogový list.
- Zkuste napsat tutoriál. 
- Pro váš produkt vyberte, jaký typ dokumentace chcete vytvářet.

## Koncept:

Otvírák na pivo je nástroj sloužící k otevírání lahví s pivem.

## Pracovní postup:

1. Vyberte si láhev piva.
2. Umístěte otvírák na uzávěr láhve.
3. Pevně držte otvírák a opatrně zvedněte uzávěr.
4. Opatrně vylévejte pivo a užijte si!

## Referenční text:
Otvírák na pivo je klasickým nástrojem pro otevírání lahví s pivem. Tento praktický nástroj se používá po celém světě a je nezbytným doplňkem pro všechny milovníky piva.

## Katalogový list:
Otvírák na pivo má ergonomický tvar pro snadné použití. Je vhodný pro otevírání lahví všech velikostí. Jeho jednoduchý design a trvanlivost ho dělají ideálním doplňkem pro vaši domácí barovou výbavu.


## Tutoriál:
Tutorial je stejný jako pracovní postup.
